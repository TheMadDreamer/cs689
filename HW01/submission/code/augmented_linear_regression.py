import numpy as np
from scipy.optimize import fmin_l_bfgs_b
from sklearn import linear_model

class AugmentedLinearRegression:
    """Augmented linear regression.

    Arguments:
        delta (float): the trade-off parameter of the loss 
    """
    def __init__(self, delta):
        self._delta = delta
        self._w = np.zeros(1,)
        self._b = 0

    def fit(self, X, y):
        """Fit the model according to the given training data.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                Real-valued output vector for training.
        
        Notes: This function must set member variables such that a subsequent call
        to get_params or predict uses the learned parameters, overwriting 
        any parameter values previously set by calling set_params.
        
        """
        (n_samples, n_features) = X.shape
        params = np.concatenate((self._w, np.array([self._b])))
        x, f, d = fmin_l_bfgs_b(self.objective, params, fprime=self.objective_grad, args=(X, y), disp=10)
        # Assign values to self variables
        self._w = x[:n_features]
        self._b = x[n_features]

        return (x, f, d)

    def predict(self, X):
        """Predict using the linear model.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)): test data

        Returns:
            y (ndarray, shape = (n_samples,): predicted values
        """
        pred = np.matmul(X, self._w) + self._b
        return pred

    def objective(self, wb, X, y):
        """Compute the objective function.

        Arguments:
            wb (ndarray, shape = (n_features + 1,)):
                concatenation of the coefficient and the bias parameters
                wb = [w, b]
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                target values.

        Returns:
            objective (float):
                the objective function evaluated on wb=[w,b] and the data X,y..
        """
        (n_samples, n_features) = X.shape
        w = wb[:n_features]
        b = wb[n_features]

        X0 = ((y - np.matmul(X, w) - b)**2)/(self._delta**2)
        obj = np.sum(((self._delta)**2)*(np.sqrt(1 + X0) - 1))
        print("Loss : ", obj)
        return obj

    def objective_grad(self, wb, X, y):
        """Compute the derivative of the objective function.

        Arguments:
            wb (ndarray, shape = (n_features + 1,)):
                concatenation of the coefficient and the bias parameters
                wb = [w, b]
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                target values.

        Returns:
            objective_grad (ndarray, shape = (n_features + 1,)):
                derivative of the objective function with respect to wb=[w,b].
        """

        (n_samples, n_features) = X.shape
        w = wb[:n_features]
        b = wb[n_features]
        error = y - np.matmul(X, w) - b
        X0 = (error**2)/(self._delta**2)

        # Grad w
        W_grad = np.sum( np.divide( -X.T*error, np.sqrt(1 + X0) ).T, 0)

        # Grad b
        b_grad = np.sum( np.divide( -1*error, np.sqrt(1 + X0) ) )

        grads = np.concatenate((np.array(W_grad), np.array([b_grad])))
        return grads

    def get_params(self):
        """Get learned parameters for the model. Assumed to be stored in
           self.w, self.b.

        Returns:
            A tuple (w,b) where w is the learned coefficients (ndarray, shape = (n_features,))
            and b is the learned bias (float).
        """
        return (self._w, self._b)

    def set_params(self, w, b):
        """Set the parameters of the model. When called, this
           function sets the model parameters tha are used
           to make predictions. Assumes parameters are stored in
           self.w, self.b.

        Arguments:
            w (ndarray, shape = (n_features,)): coefficient prior
            b (float): bias prior
        """
        self._w = w
        self._b = b

def plot_losses():
    loss = lambda x, d: (d**2)*(np.sqrt(1 + x**2/(d**2)) - 1)
    import matplotlib.pyplot as plt
    e = np.arange(-1, 1.01, 0.01)
    l1 = loss(e, 0.1)
    l2 = loss(e, 1)
    l3 = loss(e, 10)
    line1, = plt.plot(e, l1, 'b')
    line1.set_label("d = 0.1")
    line2, = plt.plot(e, l2, 'c')
    line2.set_label("d = 1")
    line3, = plt.plot(e, l3, 'g')
    line3.set_label("d = 10")
    line4, = plt.plot(e, e**2, 'r')
    line4.set_label("loss = e*e")
    plt.legend()
    plt.xlabel("error = |y - y'|")
    plt.ylabel("loss")
    plt.show()
                 


def main():
    np.random.seed(0)
    train_X = np.load('../data/q3_train_X.npy')
    train_y = np.load('../data/q3_train_y.npy')

    (n_samples, n_features) = train_X.shape

    lr = AugmentedLinearRegression(delta=1)
    lr.set_params(np.zeros(n_features), 0)
    x, _, _ = lr.fit(train_X, train_y)

    pred = lr.predict(train_X)
    mse = np.sum( (pred - train_y)**2 )
    # print("Aug Lin reg : ", mse)

    import matplotlib.pyplot as plt
    plt.scatter(train_X, train_y, 0.8)
    t = np.arange(min(train_X), max(train_X), (max(train_X) - min(train_y))/500 )
    line1, = plt.plot(t, x[:n_features]*t + x[n_features], 'r')
    line1.set_label('AugmentedLinearRegression')
    line2, = plt.plot(t, 6.403*t -1.715, 'g')
    line2.set_label('StandardLinearRegression')
    plt.legend()
    plt.xlabel("x")
    plt.ylabel("y")
    # plt.show()

def standardLinearRegression():
    np.random.seed(0)
    train_X = np.load('../data/q3_train_X.npy')
    train_y = np.load('../data/q3_train_y.npy')

    (n_samples, n_features) = train_X.shape

    lr = linear_model.LinearRegression()
    lr.fit(train_X, train_y)
    pred = lr.predict(train_X)
    mse = np.sum( (pred - train_y)**2 )
    # print("MSE Lin reg : ", mse)
    
    # print("LR params : ", lr.coef_, lr.intercept_)


if __name__ == '__main__':
    main() # 3.c
    # plot_losses() # 3.a
    # standardLinearRegression() # 3.c
