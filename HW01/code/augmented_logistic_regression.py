import numpy as np
from scipy.optimize import fmin_l_bfgs_b
from sklearn import linear_model
import scipy

class AugmentedLogisticRegression:
    """Logistic regression with optimized centering.

    Arguments:
        lambda(float): regularization parameter lambda (default: 0)
    """

    def __init__(self, lmbda=0):
        self.reg_param = lmbda  # regularization parameter (lambda)

    def fit(self, X, y):
        """Fit the model according to the given training data.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                Training output vector. Each entry is either -1 or 1.
        
        Notes: This function must set member variables such that a subsequent call
        to get_params or predict uses the learned parameters, overwriting 
        any parameter values previously set by calling set_params.
        """
        (n_samples, n_features) = X.shape
        params = np.concatenate((self._w, self._c, np.array([self._b])))
        x, f, d = fmin_l_bfgs_b(self.objective, params, fprime=self.objective_grad, args=(X, y), disp=30)
        # Assign values to self variables
        self._w = x[:n_features]
        self._c = x[n_features:2*n_features]
        self._b = x[2*n_features]

        return (x, f, d)

    def predict(self, X):
        """Predict class labels for samples in X based on current parameters.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)): test data

        Returns:
            y (ndarray, shape = (n_samples,):
                Predictions with values in {-1, +1}.
        """
        
        X_d = np.matmul(self._w, (X - self._c).T) + self._b
        
        # Calculate the probability for y=-1
        probs = np.divide(1, (1 + np.exp(X_d)))
        prob_negs = np.where(probs > 0.5)
        y = np.ones(X.shape[0])
        y[prob_negs] = -1
        return y

    def objective(self, wcb, X, y):
        # print(wcb.shape)
        # print(X.shape)
        # print(y.shape)
        """Compute the learning objective function

        Arguments:
            wcb (ndarray, shape = (2*n_features + 1,)):
                concatenation of the coefficient, centering, and bias parameters
                wcb = [w, c, b]
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                training label.

        Returns:
            objective (float):
                the objective function evaluated at [w, b, c] and the data X, y.
        """
        (n_samples, n_features) = X.shape
        w = wcb[:n_features]
        c = wcb[n_features:2*n_features]
        b = wcb[2*n_features]

        X_d = np.matmul((X - c), w) + b
        pos_Ys = np.where(y == +1)
        neg_Ys = np.where(y == -1)
        pos_too_high = np.where(X_d >= 300)
        X_d[pos_too_high] = 300
        negs_too_low = np.where(X_d <= -300)
        X_d[negs_too_low] = -300

        Z_pos = np.log(1 + np.exp(-X_d[pos_Ys]))
        Z_neg = np.log(1 + np.exp(X_d[neg_Ys]))
        obj = np.sum(Z_pos) + np.sum(Z_neg) + self.reg_param*(np.linalg.norm(w, 2)**2 + np.linalg.norm(c, 2)**2 + b**2)
        
        return obj

    def objective_grad(self, wcb, X, y):
        """Compute the gradient of the learning objective function

        Arguments:
            wcb (ndarray, shape = (2*n_features + 1,)):
                concatenation of the coefficient, centering, and bias parameters
                wcb = [w, c, b]
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                training label.

        Returns:
            objective_grad (ndarray, shape = (2*n_features + 1,)):
                gradient of the objective function with respect to [w,c,b].
        """
        (n_samples, n_features) = X.shape
        w = wcb[:n_features]
        c = wcb[n_features:2*n_features]
        b = wcb[2*n_features]

        X_d = np.matmul((X - c), w) + b
        pos_Ys = np.where(y == +1)[0]
        neg_Ys = np.where(y == -1)[0]
        pos_too_high = np.where(X_d >= 300)
        X_d[pos_too_high] = 300
        negs_too_low = np.where(X_d <= -300)
        X_d[negs_too_low] = -300
        
        W_grad_pos = np.divide(-((X[pos_Ys] - c).T*np.exp(-X_d[pos_Ys])), 1 + np.exp(-X_d[pos_Ys]))
        W_grad_neg = np.divide(((X[neg_Ys] - c).T*np.exp(X_d[neg_Ys])), 1 + np.exp(X_d[neg_Ys]))
        W_grad = np.zeros((n_features))
        W_grad = np.sum(W_grad_pos, 1) + np.sum(W_grad_neg, 1) + 2*self.reg_param*w

        c_grad_pos = w*np.sum(np.divide(np.exp(-X_d[pos_Ys]), 1 + np.exp(-X_d[pos_Ys])))
        c_grad_neg = -w*np.sum(np.divide(np.exp(X_d[neg_Ys]), 1 + np.exp(X_d[neg_Ys])))
        c_grad = np.zeros((n_features))
        c_grad = c_grad_pos + c_grad_neg + 2*self.reg_param*c

        b_grad_pos = -np.sum(np.divide(np.exp(-X_d[pos_Ys]), 1 + np.exp(-X_d[pos_Ys])))
        b_grad_neg = np.sum(np.divide(np.exp(X_d[neg_Ys]), 1 + np.exp(X_d[neg_Ys])))
        b_grad = b_grad_pos + b_grad_neg + 2*self.reg_param*b

        obj_grad = np.concatenate((W_grad, c_grad, np.array([b_grad])))
        return obj_grad

    def get_params(self):
        """Get parameters for the model.

        Returns:
            A tuple (w,c,b) where w is the learned coefficients (ndarray, shape = (n_features,)),
            c  is the learned centering parameters (ndarray, shape = (n_features,)),
            and b is the learned bias (float).
        """
        return (self._w, self._c, self._b)

    def set_params(self, w, c, b):
        """Set the parameters of the model.

        Arguments:
            w (ndarray, shape = (n_features,)): coefficients
            c (ndarray, shape = (n_features,)): centering parameters
            b (float): bias 
        """
        self._w = w
        self._c = c
        self._b = b


def main():
    np.random.seed(0)

    train_X = np.load('../data/q2_train_X.npy')
    train_y = np.load('../data/q2_train_y.npy')
    test_X = np.load('../data/q2_test_X.npy')
    test_y = np.load('../data/q2_test_y.npy')
    (n_trainX_samples, n_features) = train_X.shape
    
    lr = AugmentedLogisticRegression(lmbda = 1e-6)
    lr.set_params(0.00*np.ones(n_features), 0.00*np.ones(n_features), 0.00)
    train_X = train_X
    x, f, d =lr.fit(train_X, train_y)
    # print(f, d['warnflag'])
    w,c,b = lr.get_params()
    
    pred_test_y = lr.predict(test_X)
    TP = np.where(pred_test_y == test_y)
    accuracy = len(TP[0])/len(pred_test_y)
    print(np.max(c))
    return pred_test_y

def log_reg():
    np.random.seed(0)

    train_X = np.load('../data/q2_train_X.npy')
    train_y = np.load('../data/q2_train_y.npy')
    test_X = np.load('../data/q2_test_X.npy')
    test_y = np.load('../data/q2_test_y.npy')
    (n_trainX_samples, n_features) = train_X.shape
    
    lr = linear_model.LogisticRegression(C=1e6, solver='lbfgs', verbose=1)
    lr.fit(train_X, train_y)
    s = lr.score(test_X, test_y)
    print(s)

if __name__ == '__main__':
    main()
    # log_reg()
