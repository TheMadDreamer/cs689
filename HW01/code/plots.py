import numpy as np

def loss(e, delta):
	return (delta**2)*(np.sqrt(1 + e**2/(delta**2)) - 1)

if __name__ == "__main__":
	import matplotlib.pyplot as plt
	e = np.arange(-1, 1.01, 0.01)
	l1 = loss(e, 0.1)
	l2 = loss(e, 1)
	l3 = loss(e, 10)
	line1, = plt.plot(e, l1, 'b')
	line1.set_label("d = 0.1")
	line2, = plt.plot(e, l2, 'c')
	line2.set_label("d = 1")
	line3, = plt.plot(e, l3, 'g')
	line3.set_label("d = 10")
	line4, = plt.plot(e, e**2, 'r')
	line4.set_label("loss = e*e")
    #plt.gca().set_ylim(0,10)
	plt.legend()
	plt.xlabel("error = |y - y'|")
	plt.ylabel("loss")
	plt.show()
                 
