import argparse
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import TensorDataset
import gzip
import pickle
import os

np.random.seed(1)
torch.manual_seed(1)



class NN:
    """A network architecture for simultaneous classification 
    and angle regression of objects in images.

    Arguments:
        alpha: trade-off parameter for the composite objective function.
        epochs: number of epochs for training
    """
    def __init__(self, alpha=.5, epochs=5):
        super(NN, self).__init__()
        self.alpha = alpha
        self.epochs = epochs

        self.fc1 = nn.Linear(784, 256)
        self.fc2 = nn.Linear(256, 64)
        self.fc3_branch_class = nn.Linear(64, 32)
        self.fc3_branch_reg = nn.Linear(64, 32)
        self.fcout_branch_class = nn.Linear(32, 10)
        self.fcout_branch_reg = nn.Linear(32, 1)

    def forward(self, X):       
        # x = torch.Tensor(np.array(X, dtype=np.float32)) 
        # print(torch.matmul(x, self.fc1.weight))
        # print(X)
        fcout1 = self.fc1(X)
        # print(fcout1.shape)
        x = F.relu(self.fc1(X))
        x = F.relu(self.fc2(x))
        x_c = F.relu(self.fc3_branch_class(x))
        x_r = F.relu(self.fc3_branch_reg(x))
        x_c = F.relu(self.fcout_branch_class(x_c))
        x_r = self.fcout_branch_reg(x_r)
        return x_c, x_r

    def objective(self, X, y_class, y_angle):
        """Objective function.

        Arguments:
            X (numpy ndarray, shape = (samples, 784)):
                Input matrix where each row is a feature vector.
            y_class (numpy ndarray, shape = (samples,)):
                Labels of objects. Each entry is in [0,...,C-1].
            y_angle (numpy ndarray, shape = (samples, )):
                Angles of the objects in degrees.

        Returns:
            Composite objective function value.
        """
        num_samples, _ = X.shape
        y_class_ohv = np.zeros((num_samples, 10), dtype=np.float32)
        for elem_id, elem in enumerate(y_class):
            y_class_ohv[elem_id, y_class[elem_id]] = 1
        # print(y_class_ohv)
        # y_class_batches = np.array_split(y_class_ohv)
        # y_angle_batches = np.array_split(y_angle, num_samples/64 + 1)

        total_loss = 0
        for x in X:
            # x = torch.from_numpy(np.reshape(x, x.shape[0]))
            # print(x.shape)
            # x = torch.from_numpy(np.array(x, dtype=np.float32)) 
            # x = x.view(-1, 1, 784)
            # print(x.shape)
            # print(torch.matmul(x, self.fc1.weight.t()).shape)
            # print(fcout1.shape)
            x = torch.Tensor(np.array(x, dtype=np.float32)) 
            class_out, reg_out = self.forward(x)
            class_preds = F.softmax(class_out, dim=0)

            classification_loss = torch.sum( torch.from_numpy(y_class_ohv)*class_preds ) #np.sum( np.matmul(y_class, class_preds), 1 )
            y_angle = np.reshape(y_angle, (y_angle.shape[0], 1))
            diff = torch.from_numpy(np.array(y_angle, dtype=np.float32)) - reg_out #reg_out.sub(torch.Tensor(y_angle))
            regression_loss = torch.sum( 0.5*(1 - torch.cos(0.01745*(diff))) )
            total_loss = total_loss + (self.alpha * classification_loss + (1 - self.alpha)*regression_loss)
        # print("Classification loss :", total_loss)

        return total_loss
        
    def predict(self, X):
        """Predict class labels and object angles for samples in X.

        Arguments:
            X (numpy ndarray, shape = (samples, 784)):
                Input matrix where each row is a feature vector.

        Returns:
            y_class (numpy ndarray, shape = (samples,)):
                predicted labels. Each entry is in [0,...,C-1].
            y_angle (numpy ndarray, shape = (samples, )):
                The predicted angles of the imput objects.
        """
        x = torch.Tensor(np.array(X, dtype=np.float32)) 
        class_out, reg_out = self.forward(x)
        class_preds = F.softmax(class_out, dim=0)
        y_class = (torch.argmax(class_preds, 1)).detach().numpy()
        y_angle = (reg_out).detach().numpy()

        return y_class, y_angle

    def fit(self, X, y_class, y_angle ,step=1e-4):
        """Train the model according to the given training data.

        Arguments:
            X (numpy ndarray, shape = (samples, 784)):
                Training input matrix where each row is a feature vector.
            y_class (numpy ndarray, shape = (samples,)):
                Labels of objects. Each entry is in [0,...,C-1].
            y_angle (numpy ndarray, shape = (samples, )):
                Angles of the objects in degrees.
        """
        (num_samples, feature_size) = X.shape
        X_batches = np.array_split(X, num_samples/64 + 1)
        y_class_ohv = np.zeros((num_samples, 10))
        for elem_id, elem in enumerate(y_class):
            y_class_ohv[elem_id][y_class[elem_id]] = 1
        y_class_batches = np.array_split(y_class, num_samples/64 + 1)
        y_class_batches_ohv = np.array_split(y_class_ohv, num_samples/64 + 1)
        y_angle_batches = np.array_split(y_angle, num_samples/64 + 1)
        optimizer = optim.Adam(self.get_tensor_params(), lr=0.0001)

        # net.zero_grad()
        for epoch in np.arange(self.epochs):
            for batch_id, batch in enumerate(X_batches):
                # x = torch.from_numpy(batch)
                # print(x.shape)
                # for elem_id, elem in enumerate(batch):
                #     class_out, reg_out = self.forward(elem)
                #     class_preds = F.softmax(class_out, dim=0)
                #     y_class_ohv_ = np.zeros(10, dtype=np.float32)
                #     y_class_ohv_[y_class_batches[batch_id][elem_id]] = 1
                #     # print(y_class_ohv)
                #     # classification_loss = torch.gather(class_preds, 0, torch.Tensor(y_class_batches[batch_id][elem_id]))
                #     classification_loss = torch.sum( torch.matmul(torch.from_numpy(y_class_ohv_), class_preds) )
                #     # classification_loss = torch.sum( torch.matmul(torch.Tensor(y_class_batches[batch_id][elem_id]), class_preds) )
                    
                #     # y_angle = np.reshape(y_angle_batches[batch_id][elem_id], (y_angle_batches[batch_id][elem_id].shape[0], 1))
                #     diff = torch.Tensor([y_angle_batches[batch_id][elem_id]]) - reg_out #reg_out.sub(torch.Tensor(y_angle))
                #     regression_loss = torch.sum( 0.5*(1 - torch.cos(0.01745*(diff))) )

                #     classification_loss.backward(retain_graph=True)
                #     regression_loss.backward(retain_graph=True)
                    # optimizer.step()
                    # if (batch_id % 150 == 0):
                    #     print("epoch :", epoch, " batch :", batch_id, " Loss :", self.objective(batch, y_class_batches[batch_id*64 + elem_id], y_angle_batches[batch_id]))
                batch = torch.Tensor(np.array(batch, dtype=np.float32)) 
                class_out, reg_out = self.forward(batch)
                class_preds = F.softmax(class_out, dim=0)
                # print("Class pred : ", y_class.shape)
                classification_loss = torch.sum( torch.Tensor(y_class_batches_ohv[batch_id])*class_preds ) #np.sum( np.matmul(y_class, class_preds), 1 )
                
                # y_angle = np.reshape(y_angle_batches[batch_id], (y_angle_batches[batch_id].shape[0], 1))
                diff = torch.Tensor(y_angle_batches[batch_id]) - reg_out #reg_out.sub(torch.Tensor(y_angle))
                regression_loss = torch.sum( 0.5*(1 - torch.cos(0.01745*(diff))) )

                classification_loss.backward(retain_graph=True)
                regression_loss.backward()
                optimizer.step()
                if (batch_id % 150 == 0):
                    print("epoch :", epoch, " batch :", batch_id, " Loss :", self.objective(batch, y_class_batches[batch_id], y_angle_batches[batch_id]))

        # net.zero_grad()
        # out.backward()

    def get_tensor_params(self):
        """Get the model parameters.

        Returns:
            a list containing the following parameter values represented
            as numpy arrays (see handout for definitions of each parameter). 
        
            w1 (numpy ndarray, shape = (784, 256))
            b1 (numpy ndarray, shape = (256,))
            w2 (numpy ndarray, shape = (256, 64))
            b2 (numpy ndarray, shape = (64,))
            w3 (numpy ndarray, shape = (64, 32))
            b3 (numpy ndarray, shape = (32,))
            w4 (numpy ndarray, shape = (64, 32))
            b4 (numpy ndarray, shape = (32,))
            w5 (numpy ndarray, shape = (32, 10))
            b5 (numpy ndarray, shape = (10,))
            w6 (numpy ndarray, shape = (32, 1))
            b6 (numpy ndarray, shape = (1,))
        """
        w1 = self.fc1.weight
        b1 = self.fc1.bias
        w2 = self.fc2.weight
        b2 = self.fc2.bias
        w3 = self.fc3_branch_class.weight
        b3 = self.fc3_branch_class.bias
        w4 = self.fc3_branch_reg.weight
        b4 = self.fc3_branch_reg.bias
        w5 = self.fcout_branch_class.weight
        b5 = self.fcout_branch_class.bias
        w6 = self.fcout_branch_reg.weight
        b6 = self.fcout_branch_reg.bias

        params = [w1, b1, w2, b2, w3, b3, w4, b4, w5, b5, w6, b6]
        return params

    def get_params(self):
        """Get the model parameters.

        Returns:
            a list containing the following parameter values represented
            as numpy arrays (see handout for definitions of each parameter). 
        
            w1 (numpy ndarray, shape = (784, 256))
            b1 (numpy ndarray, shape = (256,))
            w2 (numpy ndarray, shape = (256, 64))
            b2 (numpy ndarray, shape = (64,))
            w3 (numpy ndarray, shape = (64, 32))
            b3 (numpy ndarray, shape = (32,))
            w4 (numpy ndarray, shape = (64, 32))
            b4 (numpy ndarray, shape = (32,))
            w5 (numpy ndarray, shape = (32, 10))
            b5 (numpy ndarray, shape = (10,))
            w6 (numpy ndarray, shape = (32, 1))
            b6 (numpy ndarray, shape = (1,))
        """
        w1 = (self.fc1.weight.data).detach().numpy()
        b1 = (self.fc1.bias.data).detach().numpy()
        w2 = (self.fc2.weight.data).detach().numpy()
        b2 = (self.fc2.bias.data).detach().numpy()
        w3 = (self.fc3_branch_class.weight.data).detach().numpy()
        b3 = (self.fc3_branch_class.bias.data).detach().numpy()
        w4 = (self.fc3_branch_reg.weight.data).detach().numpy()
        b4 = (self.fc3_branch_reg.bias.data).detach().numpy()
        w5 = (self.fcout_branch_class.weight.data).detach().numpy()
        b5 = (self.fcout_branch_class.bias.data).detach().numpy()
        w6 = (self.fcout_branch_reg.weight.data).detach().numpy()
        b6 = (self.fcout_branch_reg.bias.data).detach().numpy()

        params = [w1.T, b1, w2.T, b2, w3.T, b3, w4.T, b4, w5.T, b5, w6.T, b6]
        return params

    def set_params(self, params):
        """Set the model parameters.

        Arguments:
            params is a list containing the following parameter values represented
            as numpy arrays (see handout for definitions of each parameter).
        
            w1 (numpy ndarray, shape = (784, 256))
            b1 (numpy ndarray, shape = (256,))
            w2 (numpy ndarray, shape = (256, 64))
            b2 (numpy ndarray, shape = (64,))
            w3 (numpy ndarray, shape = (64, 32))
            b3 (numpy ndarray, shape = (32,))
            w4 (numpy ndarray, shape = (64, 32))
            b4 (numpy ndarray, shape = (32,))
            w5 (numpy ndarray, shape = (32, 10))
            b5 (numpy ndarray, shape = (10,))
            w6 (numpy ndarray, shape = (32, 1))
            b6 (numpy ndarray, shape = (1,))
        """
        [w1, b1, w2, b2, w3, b3, w4, b4, w5, b5, w6, b6] = params

        with torch.no_grad():
            self.fc1.weight = nn.Parameter(torch.Tensor(w1.T))
            self.fc1.bias = nn.Parameter(torch.Tensor(b1))
            # print(self.fc1.bias)
            self.fc2.weight = nn.Parameter(torch.Tensor(w2.T))
            self.fc2.bias = nn.Parameter(torch.Tensor(b2))
            self.fc3_branch_class.weight = nn.Parameter(torch.Tensor(w3.T))
            self.fc3_branch_class.bias = nn.Parameter(torch.Tensor(b3))
            self.fc3_branch_reg.weight = nn.Parameter(torch.Tensor(w4.T))
            self.fc3_branch_reg.bias = nn.Parameter(torch.Tensor(b4))
            self.fcout_branch_class.weight = nn.Parameter(torch.Tensor(w5.T))
            self.fcout_branch_class.bias = nn.Parameter(torch.Tensor(b5))
            self.fcout_branch_reg.weight = nn.Parameter(torch.Tensor(w6.T))
            self.fcout_branch_reg.bias = nn.Parameter(torch.Tensor(b6))

        return

def create_dummy_params():
        w1 = np.random.rand(784, 256)
        b1 = np.random.rand(256,)
        w2 = np.random.rand(256, 64)
        b2 = np.random.rand(64,)
        w3 = np.random.rand(64, 32)
        b3 = np.random.rand(32,)
        w4 = np.random.rand(64, 32)
        b4 = np.random.rand(32,)
        w5 = np.random.rand(32, 10)
        b5 = np.random.rand(10,)
        w6 = np.random.rand(32, 1)
        b6 = np.random.rand(1,)
        params = [w1, b1, w2, b2, w3, b3, w4, b4, w5, b5, w6, b6]
        # for elem in params:
        #     elem = np.array(elem, dtype=np.flo)
        return params

def main():
    
    DATA_DIR = '../data'
    data=np.load(os.path.join(DATA_DIR, "mnist_rot_train.npz"))
    X_tr,y_tr,a_tr = data["X"],data["labels"],data["angles"]

    data=np.load(os.path.join(DATA_DIR, "mnist_rot_validation.npz"))
    X_val,y_val,a_val = data["X"],data["labels"],data["angles"]

    #Note: test class labels and angles are not provided
    #in the data set
    data=np.load(os.path.join(DATA_DIR, "mnist_rot_test.npz"))
    X_te,y_te,a_te = data["X"],data["labels"],data["angles"]

    dummy_params = [np.double(x) for x in create_dummy_params()]
    dummy_params[-1] = np.array([dummy_params[-1]])
    
    nn = NN(0.5, 2)
    nn.set_params(dummy_params)
    nn.fit(X_tr, y_tr, a_tr)
    # nn.predict(X_val)
    # obj = nn.objective(X_tr[:64], y_tr[:64], a_tr[:64])
    # p = nn.get_params()
    # nn.set_params(p)
    
if __name__ == '__main__':
    main()