import numpy as np
from scipy.optimize import fmin_l_bfgs_b

class CountRegression:
    """Count regression.

    Arguments:
       lam (float): regaularization parameter lambda
    """
    def __init__(self, lam):
        self._lambda = lam

        # self._weights = np.zeros()
        self._bias = 0.0
    
    def fit(self, X, y):
        """Fit the model according to the given training data.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                Real-valued output vector for training.
        
        Notes: This function must set member variables such that a subsequent call
        to get_params or predict uses the learned parameters, overwriting 
        any parameter values previously set by calling set_params.
        
        """
        (n_samples, n_features) = X.shape
        self._weights = np.zeros(n_features)
        self._bias = 0 
        wb = np.append(self._weights, self._bias)
        opt, f, d = fmin_l_bfgs_b(self.objective, wb, args=(X, y), fprime=self.objective_grad, disp=10)
        # print(approx_fprime(objective))
        print(opt)
        self.set_params(opt[:n_features], opt[-1])


    def predict(self, X):
        """Predict using the odel.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)): test data

        Returns:
            y (ndarray, shape = (n_samples,): predicted values
        """
        arg = np.matmul(X, self._w) + self._b
        return np.round(np.exp(-arg))
        

    def objective(self, wb, X, y):
        """Compute the objective function.

        Arguments:
            wb (ndarray, shape = (n_features + 1,)):
                concatenation of the coefficient and the bias parameters
                wb = [w, b]
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                target values.

        Returns:
            objective (float):
                the objective function evaluated on wb=[w,b] and the data X,y..
        """
        (n_samples, n_features) = X.shape
        _w = wb[:-1]
        _b = wb[-1]
        arg = np.matmul(_w, X.T) + _b
        narg = np.matmul(_w, X.T) + _b
        narg[np.where(arg > 300)] = 300
        narg[np.where(arg < -300)] = -300
        # from scipy.misc import logsumexp
        # narg = scipy.misc.logsumexp(1 + arg)
        # likelihood_arr[np.where(arg < -300)] = narg[np.where(arg < -300)]

        likelihood_arr = - y*arg - (1 + y)*np.log(1 + np.exp(-narg))
        likelihood = np.sum(likelihood_arr, 0) - self._lambda*np.linalg.norm(_w)**2 - self._lambda*np.linalg.norm(_b)**2
        avg_likelihood = likelihood/n_samples
        print(avg_likelihood)
        return -likelihood
        

    def objective_grad(self, wb, X, y):
        """Compute the derivative of the objective function.

        Arguments:
            wb (ndarray, shape = (n_features + 1,)):
                concatenation of the coefficient and the bias parameters
                wb = [w, b]
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                target values.

        Returns:
            objective_grad (ndarray, shape = (n_features + 1,)):
                derivative of the objective function with respect to wb=[w,b].
        """
        (n_samples, n_features) = X.shape
        _w = wb[:-1]
        _b = wb[-1]
        arg = np.matmul(_w, X.T) + _b # 1 x n_samples
        narg = np.matmul(_w, X.T) + _b
        narg[np.where(arg > 300)] = 300
        narg[np.where(arg < -300)] = -300

        grad_L = np.divide((- y + np.exp(-arg)), (1 + np.exp(-arg)))
        # print(X.shape, grad_L.shape)
        # print((grad_L*X.T).shape)
        grad_w = (np.sum((grad_L*X.T).T, 0) - 2*self._lambda*_w ) #/n_samples
        grad_b = (np.sum(grad_L, 0) - 2*self._lambda*_b ) #/n_samples
        # print(grad_w, grad_b)
        return -np.append(grad_w, grad_b)


    def get_params(self):
        """Get learned parameters for the model. Assumed to be stored in
           self.w, self.b.

        Returns:
            A tuple (w,b) where w is the learned coefficients (ndarray, shape = (n_features,))
            and b is the learned bias (float).
        """
        return self._w, self._b


    def set_params(self, w, b):
        """Set the parameters of the model. When called, this
           function sets the model parameters tha are used
           to make predictions. Assumes parameters are stored in
           self.w, self.b.

        Arguments:
            w (ndarray, shape = (n_features,)): coefficient prior
            b (float): bias prior
        """
        self._w = w
        self._b = b


def main():

    data = np.load("../data/count_data.npz")
    X_train=data['X_train']
    X_test=data['X_test']
    Y_train=data['Y_train']
    Y_test=data['Y_test']

    #Define and fit model
    # print(np.mean(X_train, 0))
    # print(np.median(Y_train))
    cr  = CountRegression(1e-4)
    cr.fit(X_train,Y_train)    
    yp = cr.predict(X_test)    
    print("MSE on test :", np.mean(np.square(Y_test - yp)))
    print(cr.get_params())
    # print(X_test.shape)
    opt_w, opt_b = cr.get_params()
    print("2.d : ", (-cr.objective(np.hstack((opt_w, opt_b)), X_test, Y_test) \
                        - 1e-4*np.linalg.norm(opt_w)**2 - 1e-4*np.linalg.norm(opt_b)**2)/X_test.shape[0])

    from matplotlib import pyplot as plt
    from mpl_toolkits.mplot3d import axes3d, Axes3D
    fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    ax = Axes3D(fig)
    # print(np.array([Y_train]).shape)
    # ax.contour(X_train[:, 0], X_train[:, 1], np.array([Y_train]), 16, extend3d=True)
    surf = ax.scatter3D(X_train[:, 0], X_train[:, 1], np.array([Y_train]).T)
    # ax.contour3D(X_train[:, 0], X_train[:, 1], np.array([Y_train]))
    ax.plot([-10, 1], [10, 1], [(-10)*(-8.385e-6) + (10)*(-1.90e-6) + 1.44e-6, (1)*(-8.385e-6) + (1)*(-1.90e-6) + 1.44e-6]) #, [, ],zs=[VecStart_z[i],VecEnd_z[i]])
    # plt.plot(X_train, Y_train)
    # plt.show()
    
if __name__ == '__main__':
    main()
