import numpy as np
import gzip
import pickle


class SVM:
    """SVC with subgradient descent training.

    Arguments:
        C: regularization parameter (default: 1)
        iterations: number of training iterations (default: 500)
    """
    def __init__(self, C=1, iterations=500):
        self._C = C
        self._iters = iterations
        self._max_iters = iterations


    def fit(self, X, y):
        """ Fit the model using the training data.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                Training target. Each entry is either -1 or 1.
        
        Notes: This function must set member variables such that a subsequent call
        to get_params or predict uses the learned parameters, overwriting 
        any parameter values previously set by calling set_params.
        
        """
        (n_samples, n_features) = X.shape
        self._w = np.zeros(n_features)
        self._b = 0
        wb = np.append(self._w, self._b)

        ##### Sub gradient descent #####
        step = 0
        best_obj = 10000*self._C
        grad = np.zeros(n_features + 1)
        obj = 0
        wb_opt = wb
        losses = []
        while ((step < self._iters)): # or abs(obj - prev_obj) < epsilon):
            step_size = 0.002/np.sqrt(step + 1)
            grad = self.subgradient(wb, X, y)
            wb = wb - step_size*grad
            obj = self.objective(wb, X, y)
            losses.append(obj)
            if ((step+1) % 20000 == 0):
                print("Iter : ", step+1, " Obj : ", np.sum(obj))
            step += 1
            if (obj < best_obj):
                best_obj = obj
                wb_opt = wb 


        import matplotlib.pyplot as plt
        plt.plot(losses)
        plt.xlabel("log C")
        plt.ylabel("Train set error")
        # plt.show()
        self.set_params(wb_opt[:n_features], wb_opt[-1])
        # print(wb)
        return

       

    def objective(self, wb, X, y):
        """Compute the objective function for the SVM.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                Training target. Each entry is either -1 or 1.

        Returns:
            obj (float): value of the objective function evaluated on X and y.
        """
        (n_samples, n_features) = X.shape
        _w = wb[:-1]
        _b = wb[-1]
        # print(self._C)
        arg = 1 - y*(np.matmul(_w, X.T).T + _b)
        # print(arg.shape)
        hinge = np.zeros(arg.shape)
        hinge[np.where(arg > 0)] = arg[np.where(arg > 0)]
        obj = self._C * np.sum(hinge) + np.linalg.norm(_w, 1)
        return obj


    def subgradient(self, wb, X, y):
        """Compute the subgradient of the objective function.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)):
                Training input matrix where each row is a feature vector.
            y (ndarray, shape = (n_samples,)):
                Training target. Each entry is either -1 or 1.

        Returns:
            subgrad (ndarray, shape = (n_features+1,)):
                subgradient of the objective function with respect to
                the coefficients wb=[w,b] of the linear model 
        """
        (n_samples, n_features) = X.shape
        _w = wb[:-1]
        _b = wb[-1]
        arg = 1 - y*(np.matmul(_w, X.T).T + _b)

        first_term = np.zeros(n_features)
        first_term = - np.sum( ((np.array([y])*X.T))[:, np.where(arg > 0)], 2 )
        bias_derivate = 0
        bias_derivate = -np.sum(y[np.where(arg > 0)], 0)

        second_term = np.zeros(n_features)
        second_term[np.where(_w > 0)] = 1 
        second_term[np.where(_w < 0)] = -1 

        return np.hstack((self._C*first_term[:, 0] + second_term, self._C*bias_derivate))


    def predict(self, X):
        """Predict class labels for samples in X.

        Arguments:
            X (ndarray, shape = (n_samples, n_features)): test data

        Returns:
            y (ndarray, shape = (n_samples,):
                Predictions with values of -1 or 1.
        """
        arg = (np.matmul(self._w, X.T).T + self._b)
        ans = arg
        ans[np.where(arg >= 0)] = 1
        ans[np.where(arg < 0)] = -1
        return ans


    def get_params(self):
        """Get the model parameters.

        Returns:
            w (ndarray, shape = (n_features,)):
                coefficient of the linear model.
            b (float): bias term.
        """
        return self._w, self._b

    def set_params(self, w, b):
        """Set the model parameters.

        Arguments:
            w (ndarray, shape = (n_features,)):
                coefficient of the linear model.
            b (float): bias term.
        """
        self._w = w
        self._b = b
    

def partc():
    np.random.seed(0)

    with gzip.open('../data/svm_data.pkl.gz', 'rb') as f:
        train_X, train_y, test_X, test_y = pickle.load(f)

    clf = SVM(C=1, iterations=10000)
    clf.fit(train_X, train_y)
    preds = clf.predict(train_X)
    acc = np.zeros(len(preds))
    acc[np.where(preds == train_y)] = 1
    # print(np.sum(acc, 0)/len(preds))
    opt_params = clf.get_params()
    objvalue = clf.objective(np.append(opt_params[0], opt_params[1]), train_X, train_y)
    print("Training set objective function : ", objvalue)
    print("Training set Error fraction : ", np.sum(acc, 0)/len(preds))
    preds = clf.predict(test_X)
    acc = np.zeros(len(preds))
    acc[np.where(preds == test_y)] = 1
    objvalue = clf.objective(np.append(opt_params[0], opt_params[1]), train_X, train_y)
    print("Training set objective function : ", objvalue)
    print("Test set Error fraction : ", np.sum(acc, 0)/len(preds))

def partd():
    np.random.seed(0)

    with gzip.open('../data/svm_data.pkl.gz', 'rb') as f:
        train_X, train_y, test_X, test_y = pickle.load(f)
    
    logcs = np.arange(-3, 4)
    w_epsilon = 1e-4
    test_errors = []
    sparsity = []
    for logc in logcs:
        print("Running SVM for C : ", logc)
        clf = SVM(C = 10.**(logc), iterations=10000)
        clf.fit(train_X, train_y)
        opt_params_tuple = clf.get_params()
        opt_params = np.append(opt_params_tuple[0], opt_params_tuple[1])
        # print(opt_params_tuple.shape)
        opt_params_sparse = opt_params
        opt_params_sparse[np.where((abs(opt_params[:-1]) < w_epsilon))[0]] = 0
        # opt_params_sparse[np.where(opt_params > w_epsilon)] = 0
        # print(np.min(np.abs(opt_params)))
        # print(np.where((abs(opt_params) < w_epsilon))[0].shape)
        print( len( np.where((abs(opt_params) < w_epsilon))[0] ) )
        clf.set_params(opt_params_sparse[:-1], opt_params[-1])
        preds = clf.predict(test_X)
        acc = np.zeros( len(preds) )
        acc[ np.where(preds == test_y) ] = 1
        print( "Accuracy : ", np.sum(acc, 0)/len(preds) )
        test_errors.append(1 - (np.sum(acc, 0)/ len(preds)))

        # sparsity.append((len(np.where(np.abs(opt_params[:-1]) < w_epsilon)[0]))/ len(opt_params) )
        print( len(np.where(np.abs(clf.get_params[0]) < w_epsilon)[0]) )
        sparsity.append( len(np.where(np.abs(clf.get_params()[0]) < w_epsilon)[0])/(len(opt_params) - 1) )
    
    import matplotlib.pyplot as plt
    print(test_errors)
    fig = plt.figure()
    plt.plot(logcs, test_errors)
    plt.xlabel("log C")
    plt.ylabel("Test set error")
    plt.show()
    fig.savefig('test_errors_corrected_iter10000.png', dpi=fig.dpi)

    print(sparsity)
    fig = plt.figure()
    plt.plot(logcs, sparsity)
    plt.xlabel("log C")
    plt.ylabel("Sparsity of weights")
    plt.show()
    fig.savefig('sparsity_errors_corrected_iter10000.png', dpi=fig.dpi)


def parte():
    np.random.seed(0)
    with gzip.open('../data/svm_data.pkl.gz', 'rb') as f:
        train_X, train_y, test_X, test_y = pickle.load(f)
    
    logcs = np.arange(-3, 4)
    w_epsilon = 1e-4
    test_errors = []
    sparsity = []
    for logc in logcs:
        print("Running SVM for C : ", logc)
        clf = SVM(C = 10.**(logc), iterations=10000)
        clf.fit(train_X, train_y)
        opt_params_tuple = clf.get_params()
        opt_params = np.append(opt_params_tuple[0], opt_params_tuple[1])
        opt_params_sparse = opt_params
        opt_params_sparse[np.where((abs(opt_params[:-1]) < w_epsilon))[0]] = 0
        # print( len( np.where((abs(opt_params) < w_epsilon))[0] ) )
        clf.set_params(opt_params_sparse[:-1], opt_params[-1])
        preds = clf.predict(test_X)
        acc = np.zeros( len(preds) )
        acc[ np.where(preds == test_y) ] = 1

        print( "Accuracy : ", np.sum(acc, 0)/len(preds) )
        test_errors.append(1 - (np.sum(acc, 0)/ len(preds)))

        print( len( np.where((abs(clf.get_params()[0]) < w_epsilon))[0] ) )
        sparsity.append( len(np.where(np.abs(clf.get_params()[0]) < w_epsilon)[0])/(len(opt_params) - 1) )


        # Cross Validation
        cross_val_k = 20
        test_X_parts = np.split(test_X, cross_val_k)
        test_y_parts = np.split(test_y, cross_val_k)
        cross_val_acc = []
        for group in np.arange(cross_val_k):
            preds = clf.predict(test_X_parts[group])
            acc = np.zeros( len(preds) )
            acc[ np.where(preds == test_y_parts[group]) ] = 1
            tp = np.sum(1 - acc, 0)
            # print(tp)
            cross_val_acc.append(tp)

        variances = np.linalg.norm((np.array(cross_val_acc))/len(preds), 2)/(cross_val_k - 1)
        means = np.sum((np.array(cross_val_acc))/len(preds), 0)/cross_val_k
        print("Variance : ", variances)
        print("Mean : ", means)
        print("T Score : ", np.divide(means - 0.024, np.sqrt(variances + 0.006))*np.sqrt(cross_val_k))


    return

def main():
    # parte()
    parte()
    # np.random.seed(0)

    # with gzip.open('../data/svm_data.pkl.gz', 'rb') as f:
    #     train_X, train_y, test_X, test_y = pickle.load(f)

    # (n_samples, n_features) = train_X.shape
    # print(n_samples, n_features)
    # clf = SVM(C=1, iterations=10000)
    # clf.fit(train_X, train_y)
    # preds = clf.predict(test_X)
    # acc = np.zeros(len(preds))
    # acc[np.where(preds == test_y)] = 1
    # print(np.sum(acc, 0)/len(preds))


if __name__ == '__main__':
    main()
